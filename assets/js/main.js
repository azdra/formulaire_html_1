function Alert() {
    if(!document.getElementById("fname").value || !document.getElementById("lname").value || !document.getElementById("email").value || !document.getElementById("male").checked) return;

    let first_name = document.getElementById("fname").value.toLowerCase();
    first_name = `Prénom: ${first_name[0].toUpperCase()+first_name.slice(1)}\n`

    let last_name = document.getElementById("lname").value.toLowerCase();
    last_name = `Nom de famille: ${last_name[0].toUpperCase()+last_name.slice(1)}\n`
    
    let email = `Email: ${document.getElementById("email").value}\n`;

    let password = `Mots de passe: ${document.getElementById("password").value}\n`;

    let list = document.getElementById("list").value;
    if(list == "mort") list = "La Mort mais d'abord Tchitchi !!!";
    else list = "Tchitchi";
    list = `Vous avez choisi: ${list}\n`;

    let text = document.getElementById("textarea").value;
    if(!text) text = "Pas de message"
    text = `Message: ${text}\n`

    if(document.getElementById("male").checked) var gender = "Homme";
    if(document.getElementById("female").checked) var gender = "Femme";
    if(document.getElementById("other").checked) var gender = "Autre";

    gender = `Genre: ${gender}\n`

    if(document.getElementById("news").checked) var news = "Newsletter: Activé\n";
    else var news = "Newsletter: Non Activé\n";

    if(document.getElementById("condition").checked) var condition = "Conditions Générales: Accepté\n";
    else var condition = "Conditions Générales: Non Accepté\n";

    alert(first_name+last_name+email+password+list+text+gender+news+condition);
}